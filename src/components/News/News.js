import React, {Component} from 'react';
import axios from 'axios';
import NewSingle from './NewSingle';
import Error from './error';

class News extends Component {

    constructor(props) {
        super(props);

        this.state = {
            news: [],
            type: '',
            query: '',
            error: false
        }
    }


    componentDidUpdate(prevProps) {
        if(prevProps.query !== this.props.query){
            const url = `https://newsapi.org/v2/${this.props.type}?sources=${this.props.query}`;

            axios.get(url, {
                headers: {
                    'X-Api-Key': process.env.REACT_APP_NEWS_API_KEY
                }
            })
                .then((response) => {
                    this.setState({
                        news: response.data.articles
                    })
                })
                .catch((error) => {
                    this.setState({
                        error: true
                    })
                });
        }
    }

    componentDidMount(){

        const url = `https://newsapi.org/v2/${this.props.type}?sources=${this.props.query}`;

        axios.get(url, {
            headers: {
                'X-Api-Key': process.env.REACT_APP_NEWS_API_KEY
            }
        })
            .then((response) => {
                this.setState({
                    news: response.data.articles
                })
            })
            .catch((error) => {
                this.setState({
                    error: true
                })
            });

    }

    renderItems(){
        if(!this.state.error){
            return this.state.news.map((item) => (
                <NewSingle key={item.url} item={item} />
            ));
        } else {
            return <Error />
        }


    }

    render(){
        return (
            <div className={'row'}>
                {this.renderItems()}
            </div>
        )
    }
}

export default News;

