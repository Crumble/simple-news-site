import React, {Component} from 'react';
import axios from 'axios';

import SingleSide from './SingleSide';
import Error from './error';


class SideNews extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sideNews: [],
            error: false
        }
    }

    componentDidMount(){        
        const url = `https://newsapi.org/v2/${this.props.type}?${this.props.query}`;

        axios.get(url, {
            headers: {
                'X-Api-Key': process.env.REACT_APP_NEWS_API_KEY
            }
        })        
        .then((response) => {
            this.setState({
                sideNews: response.data.articles
            })
        })
        .catch((error) => {
            this.setState({
                error: true
            })
        });
    }

    renderItems(){
        if(!this.state.error){
            return this.state.sideNews.map((item) => (
                <SingleSide key={item.url} item={item} />
            ));
        } else {
            return <Error />
        }

    }

    render(){
        return (
            <div>
                {this.renderItems()}
            </div>
        )
    }
}

export default SideNews;

