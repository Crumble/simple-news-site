[![pipeline status](https://gitlab.com/Crumble/simple-news-site/badges/master/pipeline.svg)](https://gitlab.com/Crumble/simple-news-site/commits/master)

Requires Node v8.4 at least. untested in earlier versions.

This is a simple news site i made in React.

It uses axios to retrieve data from newsapi.org and display headlines.

You can change the source by clicking a different source in the navbar. 
Later i will update to include a searchbox and maybe more sources.

You can view the demo [here](https://crumble.gitlab.io/simple-news-site/)


How to use.

1. Clone the repo.
2. run yarn inside the project directory
3. rename .backupenv to .env
4. go to newsapi.org and get an api key.
5. replace PASTE API KEY HERE with your key inside .env
6. run yarn start
7. that's it!

