import React, { Component } from 'react';
import './App.css';
import News from './News/News';
//import SideNews from './News/SideNews';


class App extends Component {

    constructor(props){
        super(props);
        this.state = {
            type: 'top-headlines',
            query: 'techcrunch'
        };
    }

    setQuery(source){
        this.setState({
            query: source
        });
    }

  render() {
    return (
      <div className="container-fluid">
        <div className={'navbar-fixed'}>
            <nav>
                <div className={'nav-wrapper indigo lighten-4'}>
                    <a href={'/'} className={'brand-logo'}>Latest News</a>
                    <ul id={'nav-mobile'} className={'right hide-on-med-and-down'}>
                        <li><a onClick={this.setQuery.bind(this, 'google-news-au')}>Australia</a></li>
                        <li><a onClick={this.setQuery.bind(this, 'business-insider')}>Business Insider</a></li>
                        <li><a onClick={this.setQuery.bind(this, 'bbc-news')}>BBC</a></li>
                        <li><a onClick={this.setQuery.bind(this, 'buzzfeed')}>BuzzFeed</a></li>
                        <li><a onClick={this.setQuery.bind(this, 'hacker-news')}>Hacker News</a></li>
                        <li><a onClick={this.setQuery.bind(this, 'techcrunch')}>Tech Crunch</a></li>
                        <li><a onClick={this.setQuery.bind(this, 'wired')}>Wired</a></li>

                    </ul>
                </div>
            </nav>
        </div>

        <div className={'row'}>
            <div className={'col s12'}>
                <News type={this.state.type} query={this.state.query}/>
            </div>
        </div>

      </div>
    );
  }
}

export default App;
